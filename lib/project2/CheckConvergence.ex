defmodule Project2.CheckConvergence do
  def get_done_messages(0, _stat) do
    :ok
  end

  def get_done_messages(num_nodes, stat) do
    receive do
      {:done, pid} ->
        if(Map.get(stat, pid) === false) do
          # IO.inspect(pid)
          # IO.puts("Done: #{inspect(pid)}")
          stat = Map.put(stat, pid, true)
          get_done_messages(num_nodes - 1, stat)
        else
          get_done_messages(num_nodes, stat)
        end
    end
  end
end
