defmodule Project2.GossipSupervisor do
  use DynamicSupervisor

  def start_link(opts \\ []) do
    DynamicSupervisor.start_link(__MODULE__, :ok, opts)
  end

  def start_agent(sup_process) do
    DynamicSupervisor.start_child(sup_process, Project2.NodeMap)
  end

  def start_nodes(
        _main_process,
        {_sup_process, _map_process},
        {0, _num_nodes},
        _topology,
        _time_period,
        _algorithm,
        _rand_2D_process,
        stat
      ) do
    stat
  end

  def start_nodes(
        main_process,
        {sup_process, map_process},
        {n, num_nodes},
        topology,
        time_period,
        algorithm,
        rand_2D_process,
        stat
      ) do
    {:ok, rand_2D_process} =
      if(n == num_nodes && topology === :random_2D_grid) do
        DynamicSupervisor.start_child(sup_process, Project2.NodeMap)
      else
        {:ok, rand_2D_process}
      end

    state = %{
      :node_idx => n,
      :num_nodes => num_nodes,
      :map_process => map_process,
      :topology => topology,
      :time_period => time_period,
      :main_process => main_process,
      :algorithm => algorithm,
      :rand_2D_process => rand_2D_process
    }

    {:ok, pid} = DynamicSupervisor.start_child(sup_process, {Project2.Node, state})

    Project2.NodeMap.put(map_process, n, pid)

    stat = Map.put(stat, pid, false)

    Project2.GossipSupervisor.start_nodes(
      main_process,
      {sup_process, map_process},
      {n - 1, num_nodes},
      topology,
      time_period,
      algorithm,
      rand_2D_process,
      stat
    )
  end

  # Callbacks
  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
