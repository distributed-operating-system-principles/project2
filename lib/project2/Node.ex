defmodule Project2.Node do
  use GenServer
  # -----------------------------------------------------------------------------------------
  def start_link(state, opts \\ []) do
    GenServer.start_link(__MODULE__, state, opts)
  end

  def infect_node(process, {s, w}) do
    GenServer.cast(process, {:push_sum, {s, w}})
  end

  def infect_node(process, rumor) do
    GenServer.cast(process, {:infect_node, rumor})
  end

  # -----------------------------------------------------------------------------------------
  # Callbacks
  def init(state) do
    state =
      if(Map.get(state, :algorithm) === :push_sum) do
        state = Map.put(state, :rumor, {Map.get(state, :node_idx), 1})
        state = Map.put(state, :rounds, 0)
        state = Map.put(state, :avg, 0)
        Process.send_after(self(), :continue_push_sum, Map.get(state, :time_period))
        state
      else
        state
      end

    state =
      if(Map.get(state, :topology) === :imperfect_line) do
        node_idx = Map.get(state, :node_idx)
        num_nodes = Map.get(state, :num_nodes)
        rand_neighbor = get_random_imperfect_line(node_idx, num_nodes)
        Map.put(state, :rand_neighbor, rand_neighbor)
      else
        state
      end

    state =
      if(Map.get(state, :topology) === :random_2D_grid) do
        x = (:rand.uniform(100 + 1) - 1) / 100
        y = (:rand.uniform(100 + 1) - 1) / 100
        rand_2D_process = Map.get(state, :rand_2D_process)
        node_idx = Map.get(state, :node_idx)
        Project2.NodeMap.put(rand_2D_process, node_idx, {x, y})
        Map.put(state, :rand_2D_coords, {x, y})
      else
        state
      end

    {:ok, state}
  end

  def handle_cast({:push_sum, {s, w}}, state) do
    state =
      Map.put(
        state,
        :rumor,
        {elem(Map.get(state, :rumor), 0) + s, elem(Map.get(state, :rumor), 1) + w}
      )

    {:noreply, state}
  end

  def handle_cast({:infect_node, rumor}, state) do
    exist_rumor = Map.get(state, :rumor)

    state =
      if(exist_rumor === nil) do
        temp = Map.put(state, :rumor, rumor)
        send(Map.get(state, :main_process), {:done, self()})
        Process.send_after(self(), :continue_gossip, Map.get(state, :time_period))
        Map.put(temp, :recvd_count, 0)
      else
        recvd_count = Map.get(state, :recvd_count)
        Map.put(state, :recvd_count, recvd_count + 1)
      end

    {:noreply, state}
  end

  def handle_info(:continue_push_sum, state) do
    {state_s, state_w} = Map.get(state, :rumor)
    # IO.puts("#{Map.get(state, :node_idx)} : #{state_s / state_w}")
    prev_avg = Map.get(state, :avg)
    rounds = Map.get(state, :rounds)
    new_avg = state_s / state_w
    state = Map.put(state, :avg, new_avg)

    # IO.puts("#{Map.get(state, :node_idx)} #{new_avg}")

    rounds =
      if(abs(new_avg - prev_avg) < 1.0e-10) do
        rounds + 1
      else
        0
      end

    state = Map.put(state, :rounds, rounds)

    state =
      if(rounds >= 4) do
        # IO.puts("#{Map.get(state, :node_idx)} #{new_avg}")
        send(Map.get(state, :main_process), {:done, self()})
        state
      else
        state = Map.put(state, :rumor, {state_s / 2, state_w / 2})
        state = send_rumor(state)
        Process.send_after(self(), :continue_push_sum, Map.get(state, :time_period))
        state
      end

    {:noreply, state}
  end

  def handle_info(:continue_gossip, state) do
    recvd_count = Map.get(state, :recvd_count)

    state =
      if(recvd_count <= 10) do
        state = send_rumor(state)
        Process.send_after(self(), :continue_gossip, Map.get(state, :time_period))
        state
      else
        state
      end

    {:noreply, state}
  end

  def handle_info(_msg, state) do
    {:noreply, state}
  end

  # --------------------------------------------------------------------------------------------

  # Private Methods
  defp send_rumor(state) do
    topology = Map.get(state, :topology)

    state =
      case topology do
        :full_network -> send_in_full_network(state)
        :grid_3D -> send_in_3D_grid(state)
        :random_2D_grid -> send_in_random_2D_grid(state)
        :toroid -> send_in_toroid(state)
        :line -> send_in_line(state)
        :imperfect_line -> send_in_imperfect_line(state)
        _ -> state
      end

    state
  end

  defp send_in_full_network(state) do
    num_nodes = Map.get(state, :num_nodes)
    node_idx = Map.get(state, :node_idx)
    rumor = Map.get(state, :rumor)
    node_map = Map.get(state, :map_process)
    send_to = get_random_full_network(node_idx, num_nodes)
    process = Project2.NodeMap.get(node_map, send_to)
    Project2.Node.infect_node(process, rumor)
    state
  end

  defp send_in_3D_grid(state) do
    num_nodes = Map.get(state, :num_nodes)
    node_idx = Map.get(state, :node_idx)
    map_process = Map.get(state, :map_process)
    rumor = Map.get(state, :rumor)
    n = :math.pow(num_nodes, 1 / 3) |> :math.ceil() |> Kernel.trunc()

    # IO.puts(n)

    {x, y, z} = {rem(node_idx - 1, n), div(node_idx - 1, n) |> rem(n), div(node_idx - 1, n * n)}

    neighbors = []
    neighbors = [{x - 1, y, z} | neighbors]
    neighbors = [{x + 1, y, z} | neighbors]
    neighbors = [{x, y - 1, z} | neighbors]
    neighbors = [{x, y + 1, z} | neighbors]
    neighbors = [{x, y, z - 1} | neighbors]
    neighbors = [{x, y, z + 1} | neighbors]

    res = get_random_grid_3D(neighbors, map_process, n)
    {x, y, z} = Enum.at(neighbors, res)
    val = x + y * n + z * n * n + 1
    process = Project2.NodeMap.get(map_process, val)
    # IO.puts("#{inspect(self())} : #{inspect(process)} ; #{node_idx} : #{val}")

    Project2.Node.infect_node(process, rumor)

    state
  end

  defp send_in_random_2D_grid(state) do
    neighbors = Map.get(state, :rand_2D_neighbors)
    num_nodes = Map.get(state, :num_nodes)
    node_idx = Map.get(state, :node_idx)
    map_process = Map.get(state, :map_process)
    rumor = Map.get(state, :rumor)

    {neighbors, c} =
      if(neighbors === nil) do
        c = 0
        get_random_2D_neighbors(state, num_nodes, [], c)
      else
        {neighbors, Map.get(state, :num_2D_neighbors)}
      end

    state = Map.put(state, :rand_2D_neighbors, neighbors)
    state = Map.put(state, :num_2D_neighbors, c)
    rand_no = get_random_2D_grid(node_idx, neighbors, c)
    send_to = Enum.at(neighbors, rand_no)
    process = Project2.NodeMap.get(map_process, send_to)
    Project2.Node.infect_node(process, rumor)
    state
  end

  defp send_in_toroid(state) do
    node_idx = Map.get(state, :node_idx)
    num_nodes = Map.get(state, :num_nodes)
    map_process = Map.get(state, :map_process)
    rumor = Map.get(state, :rumor)
    n = :math.pow(num_nodes, 1 / 2) |> :math.ceil() |> Kernel.trunc()
    {x, y} = {rem(node_idx - 1, n), div(node_idx - 1, n)}
    neighbors = []
    neighbors = [{x, rem(n + y - 1, n)} | neighbors]
    neighbors = [{x, rem(y + 1, n)} | neighbors]
    neighbors = [{rem(n + x - 1, n), y} | neighbors]
    neighbors = [{rem(x + 1, n), y} | neighbors]

    send_to = get_random_toroid(neighbors, n, num_nodes)
    {x, y} = Enum.at(neighbors, send_to)
    process = Project2.NodeMap.get(map_process, x + n * y + 1)
    Project2.Node.infect_node(process, rumor)

    state
  end

  defp send_in_line(state) do
    num_nodes = Map.get(state, :num_nodes)
    node_idx = Map.get(state, :node_idx)
    rumor = Map.get(state, :rumor)
    node_map = Map.get(state, :map_process)

    send_node =
      case node_idx do
        1 ->
          2

        ^num_nodes ->
          num_nodes - 1

        n when n > 0 ->
          Enum.random([n - 1, n + 1])
          # nil ->
      end

    # IO.puts("neighbor of node index #{Map.get(state, :node_idx)} is #{send_node}")
    process = Project2.NodeMap.get(node_map, send_node)
    Project2.Node.infect_node(process, rumor)
    state
  end

  defp send_in_imperfect_line(state) do
    num_nodes = Map.get(state, :num_nodes)
    node_idx = Map.get(state, :node_idx)
    rumor = Map.get(state, :rumor)
    node_map = Map.get(state, :map_process)

    rand_neighbor = Map.get(state, :rand_neighbor)

    c = 1
    neighbors = [rand_neighbor]

    {neighbors, c} =
      case node_idx do
        1 ->
          {[2 | neighbors], c + 1}

        ^num_nodes ->
          {[num_nodes - 1 | neighbors], c + 1}

        n when n > 0 ->
          neighbors = [n - 1 | neighbors]
          {[n + 1 | neighbors], c + 2}
          # nil ->
      end

    process = Project2.NodeMap.get(node_map, Enum.at(neighbors, :rand.uniform(c) - 1))
    Project2.Node.infect_node(process, rumor)
    state
  end

  # --------------------------------------------------------------------------------------------
  defp get_random_full_network(node_idx, num_nodes) do
    rand_v = :rand.uniform(num_nodes)

    res =
      if(rand_v === node_idx) do
        get_random_full_network(node_idx, num_nodes)
      else
        rand_v
      end

    res
  end

  defp get_random_imperfect_line(node_idx, num_nodes) do
    rand_v = :rand.uniform(num_nodes)

    res =
      if(rand_v === node_idx || rand_v === node_idx - 1 || rand_v === node_idx + 1) do
        get_random_imperfect_line(node_idx, num_nodes)
      else
        rand_v
      end

    res
  end

  defp get_random_toroid(neighbors, n, num_nodes) do
    res = :rand.uniform(4) - 1
    {x, y} = Enum.at(neighbors, res)

    res =
      if(x + y * n + 1 > num_nodes) do
        get_random_toroid(neighbors, n, num_nodes)
      else
        res
      end

    res
  end

  defp get_random_grid_3D(neighbors, map_process, n) do
    res = :rand.uniform(6) - 1
    {x, y, z} = Enum.at(neighbors, res)
    val = x + y * n + z * n * n + 1
    process = Project2.NodeMap.get(map_process, val)

    res =
      if(x >= 0 && x < n && (y >= 0 && y < n) && (z >= 0 && z < n) && process !== nil) do
        res
      else
        get_random_grid_3D(neighbors, map_process, n)
      end

    res
  end

  defp get_random_2D_grid(node_idx, rand_2D_neighbors, c) do
    rand_no = :rand.uniform(c) - 1
    nb = Enum.at(rand_2D_neighbors, rand_no)

    res =
      if(nb === node_idx) do
        get_random_2D_grid(node_idx, rand_2D_neighbors, c)
      else
        rand_no
      end

    res
  end

  defp get_random_2D_neighbors(_state, 0, neighbors, c) do
    # IO.inspect(neighbors)
    {neighbors, c}
  end

  defp get_random_2D_neighbors(state, num_nodes, neighbors, c) do
    rand_2D_process = Map.get(state, :rand_2D_process)
    {x_i, y_i} = Map.get(state, :rand_2D_coords)
    {x, y} = Project2.NodeMap.get(rand_2D_process, num_nodes)
    comp = (:math.pow(x_i - x, 2) + :math.pow(y_i - y, 2)) |> :math.sqrt()

    {neighbors, c} =
      if(comp < 0.1) do
        {[num_nodes | neighbors], c + 1}
      else
        {neighbors, c}
      end

    get_random_2D_neighbors(state, num_nodes - 1, neighbors, c)
  end
end
