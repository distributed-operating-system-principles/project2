defmodule Project2.NodeMap do
  use Agent

  def start_link(opts \\ []) do
    Agent.start_link(fn -> %{} end, opts)
  end

  def infect_child_node(map_process, child_idx, rumor) do
    child_pid = Project2.NodeMap.get(map_process, child_idx)
    Project2.Node.infect_node(child_pid, rumor)
  end

  def get(map_process, key) do
    Agent.get(map_process, &Map.get(&1, key))
  end

  def put(map_process, key, value) do
    Agent.update(map_process, &Map.put(&1, key, value))
  end

  def delete(map_process, key) do
    Agent.get_and_update(map_process, &Map.pop(&1, key))
  end
end
