# Description

Gossip type algorithms can be used both for group communication and for aggregate computation. The goal of this project is to determine the convergence of such algorithms through a simulator based on actors written in Elixir. Since actors in Elixir are fully asynchronous, the particular type of Gossip implemented is the so called Asynchronous Gossip.

**Gossip Algorithm**

Gossip Algorithm for information propagation. The Gossip algorithm involves the following:

• *Starting*: A participant(actor) it told/sent a roumor(fact) by the main process

• *Step*: Each actor selects a random neighboor and tells it the roumor

• *Termination*: Each actor keeps track of rumors and how many times it has heard the rumor. It stops transmitting once it has heard the roumor 10 times (10 is arbitrary).

**Push-Sum Algorithm**

Push-Sum algorithm for sum computation

• *State*: Each actor Ai maintains two quantities: s and w. Initially, s = xi = i (that is actor number i has value i) and w = 1

• *Starting*: Everyone of the actors starts the algorithm in the beginning.

• *Receive*: Messages sent and received are pairs of the form (s, w). Upon receive from any other actor, an actor should add received pair to its own corresponding values. 

• *Send*: At the start of every round of an actor, half of s and w is kept by the sending actor and half is placed in the message.

• *Sum estimate*: At any given moment of time, the sum estimate is s/w
where s and w are the current values of an actor.

• *Termination*: If an actor's ratio s/w did not change more than 10<sup>-10</sup> in 3 consecutive rounds the actor terminates.

**Topologies**

The actual network topology plays a critical role in the dissemination speed of Gossip protocols. The topology determines who is considered a neighboor in the above algorithms.

• *Full Network*: Every actor is a neighbor of all other actors. That is, every actor can talk directly to any other actor.

• *3D Grid*: Actors form a 3D grid. The actors can only talk to the grid neigboors.

• *Random 2D Grid*: Actors are randomly position at x,y coordinnates on a [0-1.0]X[0-1.0] square. Two actors are connected if they are within .1 distance to other actors.

• *Torus*: Actors are arranged in a torus. That is, each actor has 4 neighbors (similar to a 2D grid) but both directions are closed to form circles.

• *Line*: Actors are arranged in a line. Each actor has only 2 neighbors (one left and one right, unless you are the first or last actor).

• *Imperfect Line*: Line arrangement but one random other neighboor is selected from the list of all actors.

**Instructions**

- Open Project2 directory
- Open terminal within this directory
- Run "mix compile"
- Run "mix run project2.exs <*numNodes*> <*topology*> <*algorithm*>"
- Topology is one of: *full*, *3D*, *imp2D*, *torus*, *rand2D* and *line*.
- Algorithm is one of: *gossip* and *push-sum*
- To get the time taken to run the code, we used the below command

	"time mix run project2.exs <*numNodes*> <*topology*> <*algorithm*>"
- Real time from above command is the time we measured for convergence

**Observations**

- We achieved convergence for all the topologies for varied number of nodes, for both gossip and push-sum protocols.

   Topologies used: Full network, 3D grid, Torus, Line, Imperfect line, Random 2D grid

- The largest networks we used are:
	    
        Gossip Protocol
    
	|   Topology	|   Number of Nodes		|   Convergence Time(s)|
    |---------------|-----------------------|----------------------|
	|   Full		|   200000			    |   16.732             |
    |   3D          |   200000              |   43.865             |
    |   rand2D      |   10000               |   236.894            |
    |   Torus       |   200000              |   126.826            |
    |   Line        |   1000                |   150.942            |
    |   ImpLine     |   100000              |   12.362             |

    	Push-sum Protocol

	|   Topology	|   Number of Nodes		|   Convergence Time(s)|
    |---------------|-----------------------|----------------------|
	|   Full		|   200000			    |   11.415             |
    |   3D          |   200000              |   59.423             |
    |   rand2D      |   10000               |   246.294            |
    |   Torus       |   200000              |   50.881             |
    |   Line        |   1000                |   72.749             |
    |   ImpLine     |   200000              |   29.025             |

**Gossip Protocol Observations**

- We observed that the order of convergence times is
    
    t(full) < t(imp2D) < t(3D) < t(torus) < t(rand2D) < t(line)

- The random2D didn’t converge for Nodes below 400. It is because, for nodes below 400, there were more than one connected components (some nodes do not have neighbors at all that fall within 0.1 distance of themselves). Thus some of the nodes didn’t receive the rumor.

- In Random 2D grid topology, each node calculates its set of neighbors in the beginning by traversing the whole set of nodes for their x, y coordinates. This is an overhead. This is the reason that the random 2D grid is slower than the other topologies.

- The line topology didn’t converge within sufficient time for nodes above 10000

- The imperfect 2D line topology converges significantly faster than the line topology. This is because the only way the rumor propagates is not linearly. It involves a random neighbor that propagates the rumor into the network.

- The imperfect 2D line topology converges most of the time. However for large number of nodes when the random neighbors selected by the nodes form a topology not significantly different from a line topology, the time taken to converge was higher.

**Push Sum Protocol Observations**

- We observed that the order of convergence times is

    t(full) < t(imp2D) < t(torus) < t(3D) < t(line) < t(rand2D)

- In line topology, the algorithm converges quicker than the gossip counterpart, because this algorithm is initiated at all the nodes. In gossip, the algorithm is started at a single node and the rumor has to be propagated throughout the network in only two directions from a node.

- Below are the average values that the nodes converged to :

    Full topology → 500.5 (For 1000 nodes) 

    Random2D → 499.05 (For 1000 nodes) 

    Torus topology→ 51.43 (For 100 nodes) 

    Line topology → 51.11 (For 100 nodes) 

    3D topology → 50.57 (For 100 nodes) 

    Imp2D → 50.75 (For 100 nodes)

- In case of Full Network, the average values that the nodes converged to has always been consistent with the expected value.

- In line topology every node converges at an average value which is near to its initial value of the sum (that is its own index) because the line topology is more localized in nature.

- For smaller number of nodes, all topologies converge to the expected average value (s / w) in the appropriate manner.

- We tested the push sum protocol on the average values not changing more than 3 consecutive rounds and also 6 consecutive rounds. We observed that the protocol took a longer time to converge when we were testing for 6 consecutive rounds.

**Graphs** (*Created using the Google Charts library in Javascript*)

![Gossip](Gossip.png)

![Push-Sum](Push-Sum.png)