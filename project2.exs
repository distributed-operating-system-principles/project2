# topologies-> :full_network, :grid_3D, :random_2D_grid, :toroid, :line, :imperfect_line
# algorithms-> :gossip, :push_sum
time_out = 100

args = System.argv()
num_nodes = elem(Integer.parse(Enum.at(args, 0)), 0)

topology =
  case Enum.at(args, 1) do
    "full" -> :full_network
    "3D" -> :grid_3D
    "rand2D" -> :random_2D_grid
    "torus" -> :toroid
    "line" -> :line
    "imp2D" -> :imperfect_line
    _ -> :ok
  end

algorithm =
  case Enum.at(args, 2) do
    "gossip" -> :gossip
    "push-sum" -> :push_sum
    _ -> :ok
  end

# IO.inspect({num_nodes, topology, algorithm})

# num_nodes = 500
# topology = :full_network
# algorithm = :push_sum

{:ok, p_id} = Project2.GossipSupervisor.start_link()
{:ok, map_process} = Project2.GossipSupervisor.start_agent(p_id)

Process.flag(:trap_exit, true)

stat =
  Project2.GossipSupervisor.start_nodes(
    self(),
    {p_id, map_process},
    {num_nodes, num_nodes},
    topology,
    time_out,
    algorithm,
    nil,
    %{}
  )

# :rand.uniform(num_nodes)
first_node = :rand.uniform(num_nodes)

if(algorithm === :gossip) do
  Project2.NodeMap.infect_child_node(map_process, first_node, "I am Prabhakar")
end

Process.exit(Enum.at(stat, 17), :kill)

Project2.CheckConvergence.get_done_messages(num_nodes, stat)

Process.exit(p_id, :kill)
